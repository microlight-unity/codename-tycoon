*****Components
id = ID in database
name= Name shown to player
description= Description of CPU shown to player
componenttype = Type of component it is (0 = CPU, 1 = GPU, 2 = SPU, 3 = RAM)
forconsoletype = For which type of console it is (0 = Home, 1 = Handheld, 2 = Both)
productioncost = How much it costs to produce part for console (production phase)
productiontime = How much time is needed for this part in console production
implementationskill = Required hardware skill to work on this component
implementationcost = How much it costs to implement this part in console design phase
implementationtime = How much time is needed to implement this part in console design phase
power = Power of this part (different meanings for different components, for PSU it means how much power it can supply, in CPU tells how strong CPU is)
requiredpower = How much power is required by this part
heat = How much heat this part generates
maxtemp = Maximum temperature this part can handle before it starts to drop in performance (or stop working completely)
imageid = ID of image that will be shown and used for this component