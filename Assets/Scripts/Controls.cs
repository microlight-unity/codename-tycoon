using UnityEngine;

// Takes player inputs and controls
public class Controls : MonoBehaviour {
    ReferenceDatabase rd;

    private void Awake() {
        rd = ReferenceDatabase.Instance;
    }

    private void Update() {
        if(Input.GetKeyDown(KeyCode.Alpha1) || Input.GetKeyDown(KeyCode.Keypad1)) {
            NormalSpeed();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2) || Input.GetKeyDown(KeyCode.Keypad2)) {
            FastSpeed();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3) || Input.GetKeyDown(KeyCode.Keypad3)) {
            ExtraSpeed();
        }

        if (Input.GetKeyDown(KeyCode.Alpha0) || Input.GetKeyDown(KeyCode.Space)) {
            PauseSpeed();
        }
    }

    // Speed controls
    public void NormalSpeed() {
        rd.tc.ChangeSpeed(TickSpeed.Normal);
    }
    public void FastSpeed() {
        rd.tc.ChangeSpeed(TickSpeed.Fast);
    }
    public void ExtraSpeed() {
        rd.tc.ChangeSpeed(TickSpeed.Extra);
    }
    public void PauseSpeed() {
        rd.tc.PauseToggle();
    }
}