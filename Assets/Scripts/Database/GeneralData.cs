public class GeneralData {
    int playerCompany;
    int bankCompany;
    int bankAccount;

    // Singleton
    private static GeneralData _instance = null;
    public static GeneralData Instance {
        get {
            if (_instance == null) {
                _instance = new GeneralData();
            }

            return _instance;
        }
    }

    GeneralData() {
        CompanyDB cdb = CompanyDB.Instance;

        bankCompany = cdb.CreateCompany(ConfigBank.BankName);
        bankAccount = cdb.GetCompany(bankCompany).GetBankAccountID();
        playerCompany = cdb.CreateCompany("Player company");
    }

    #region Getters
    public int GetPlayerCompany() {
        return playerCompany;
    }
    public int GetBankCompany() {
        return bankCompany;
    }
    public int GetBankAccount() {
        return bankAccount;
    }
    #endregion

    #region Setters
    public void SetPlayerCompany(int playerCompany) {
        this.playerCompany = playerCompany;
    }
    public void SetBankCompany(int bankCompany) {
        this.bankCompany = bankCompany;
    }
    public void SetBankAccount(int bankAccount) {
        this.bankAccount = bankAccount;
    }
    #endregion
}
