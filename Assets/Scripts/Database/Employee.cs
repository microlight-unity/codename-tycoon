using System.Collections.Generic;
using UnityEngine;

public class EmployeeDB {
    Dictionary<int, Employee> employees;
    List<string> maleNames;
    List<string> femaleNames;
    List<string> lastNames;

    // Singleton
    private static EmployeeDB _instance = null;
    public static EmployeeDB Instance {
        get {
            if (_instance == null) {
                _instance = new EmployeeDB();
            }

            return _instance;
        }
    }

    // Contructor
    EmployeeDB() {
        employees = new Dictionary<int, Employee>();

        maleNames = new List<string>(Utility.ImportTextFile("/Names/MaleNames.txt", false, false));
        femaleNames = new List<string>(Utility.ImportTextFile("/Names/FemaleNames.txt", false, false));
        lastNames = new List<string>(Utility.ImportTextFile("/Names/LastNames.txt", false, false));
    }

    #region Getters
    // Variables
    public IReadOnlyDictionary<int, Employee> GetEmployees() {
        return employees;
    }
    public Employee GetEmployee(int key) {
        if (!employees.ContainsKey(key)) {
            MDebug.Warning("EmployeeDB", "GetEmployee", "Employee doesn't exist!", new object[] { key });
            return null;
        }

        return employees[key];
    }

    public string GetRandomMaleName() {
        return maleNames[Random.Range(0, maleNames.Count)];
    }
    public string GetRandomFemaleName() {
        return femaleNames[Random.Range(0, femaleNames.Count)];
    }
    public string GetRandomLastName() {
        return lastNames[Random.Range(0, lastNames.Count)];
    }
    #endregion
}

public enum Education {
    Primary,
    Bachelor,
    Master
}

public class Employee {
    // General
    string firstName;
    string lastName;
    bool isMale;
    int dob;
    Education educationLevel;

    // Employment
    int employedCompany;
    int team;

    // Portrait
    int portrait;

    // Constructor
    public Employee(string firstName, string lastName, bool isMale, int dob, Education educationLevel, int portrait) {
        // General
        this.firstName = firstName;
        this.lastName = lastName;
        this.isMale = isMale;
        this.dob = dob;
        this.educationLevel = educationLevel;

        // Employment
        employedCompany = -1;
        team = -1;

        // Portrait
        this.portrait = portrait;
    }

    #region Getters
    // General
    public string GetFirstName() {
        return firstName;
    }
    public string GetLastName() {
        return lastName;
    }
    public bool GetIsMale() {
        return isMale;
    }
    public int GetDOB() {
        return dob;
    }
    public Education GetEducationLevel() {
        return educationLevel;
    }

    // Employment
    public int GetEmployedCompany() {
        return employedCompany;
    }
    public int GetTeam() {
        return team;
    }

    // Portrait
    public int GetPortrait() {
        return portrait;
    }
    #endregion
}
