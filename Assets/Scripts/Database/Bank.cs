using System.Collections.Generic;

public class Bank {
    Dictionary<int, BankAccount> accounts;
    Dictionary<int, Transaction> transactions;

    // Singleton
    private static Bank _instance = null;
    public static Bank Instance {
        get {
            if (_instance == null) {
                _instance = new Bank();
            }

            return _instance;
        }
    }

    // Constructor
    Bank() {
        accounts = new Dictionary<int, BankAccount>();
        transactions = new Dictionary<int, Transaction>();
    }

    #region Getters
    public IReadOnlyDictionary<int, BankAccount> GetAccounts() {
        return accounts;
    }
    public BankAccount GetAccount(int key) {
        if (accounts.ContainsKey(key)) {
            return accounts[key];
        }

        MDebug.Warning("Bank", "GetAccount", "Bank account doesn't exist!", new object[] { key });
        return null;
    }
    public IReadOnlyDictionary<int, Transaction> GetTransactions() {
        return transactions;
    }
    public Transaction GetTransaction(int key) {
        if (transactions.ContainsKey(key)) {
            return transactions[key];
        }

        MDebug.Warning("Bank", "GetTransaction", "Transaction doesn't exist!", new object[] { key });
        return null;
    }
    #endregion

    // Creates new account
    public int CreateAccount(int companyID) {
        // Finds new unique ID
        int id = Utility.GenerateID();
        while (accounts.ContainsKey(id)) {
            id = Utility.GenerateID();
        }

        // CompanyID value is trusted cause bank account is created DURING company creation
        accounts.Add(id, new BankAccount(companyID));

        return id;
    }

    public int TransferMoney(int senderAccount, int recieverAccount, double amount, TransactionType transactionType, int dob) {
        // Check if accounts exist
        if (!accounts.ContainsKey(senderAccount)) {
            MDebug.Error("Bank", "TransferMoney", "Sender not found!", new object[] { senderAccount });
        }
        if (!accounts.ContainsKey(recieverAccount)) {
            MDebug.Error("Bank", "TransferMoney", "Reciever not found!", new object[] { recieverAccount });
        }

        int transactionID = Utility.GenerateID();
        while (transactions.ContainsKey(transactionID)) {
            transactionID = Utility.GenerateID();
        }
        transactions.Add(transactionID, new Transaction(senderAccount, recieverAccount, amount, dob, transactionType));
        GetAccount(recieverAccount).SetBalance(amount, true);
        GetAccount(senderAccount).SetBalance(-amount, true);
        return transactionID;
    }
}

public class BankAccount {
    // General
    int company;
    double balance;
    List<Loan> loans;    

    // Constructor
    public BankAccount(int company) {
        this.company = company;
        this.balance = 0;
        loans = new List<Loan>();
    }

    #region Getters
    public int GetCompany() {
        return company;
    }
    public double GetBalance() {
        return balance;
    }
    public IReadOnlyList<Loan> GetLoans() {
        return loans;
    }
    #endregion

    #region Setters
    public void SetBalance(double amount, bool add = false) {
        // If "add" then amount is added instead of set
        if (add) {
            balance += amount;
        }
        else {
            balance = amount;
        }
    }
    #endregion
}

public class Loan {
    // General
    bool active;
    double loanAmount;
    double loanInterest;
    int loanTime;

    // Repay
    int timeLeft;
    double weeklyCapitalReturn;
    double weeklyInterest;

    // Constructor
    public Loan (double loanAmount, double loanInterest, int loanTime, double weeklyCapitalReturn, double weeklyInterest) {
        active = true;
        this.loanAmount = loanAmount;
        this.loanInterest = loanInterest;
        this.loanTime = loanTime;

        timeLeft = loanTime;
        this.weeklyCapitalReturn = weeklyCapitalReturn;
        this.weeklyInterest = weeklyInterest;
    }

    #region Getters
    public bool IsActive() {
        return active;
    }
    public double GetLoanAmount() {
        return loanAmount;
    }
    public double GetLoanInterest() {
        return loanInterest;
    }
    public int GetLoanTime() {
        return loanTime;
    }

    // Repay
    public int GetTimeLeft() {
        return timeLeft;
    }
    public double GetWeeklyCapitalReturn() {
        return weeklyCapitalReturn;
    }
    public double GetWeeklyInterest() {
        return weeklyCapitalReturn;
    }
    public double GetWeeklyRate() {
        return weeklyCapitalReturn + weeklyInterest;
    }
    #endregion
}

public enum TransactionType {
    General,
    Loan
}

public class Transaction {
    // General
    int senderAccount;
    int recieverAccount;
    double amount;
    int dob;

    TransactionType transactionType;

    // Constructor
    public Transaction(int senderAccount, int recieverAccount, double amount, int dob, TransactionType transactionType) {
        this.senderAccount = senderAccount;
        this.recieverAccount = recieverAccount;
        this.amount = amount;
        this.dob = dob;

        this.transactionType = transactionType;
    }

    #region Getters
    public int GetSender() {
        return senderAccount;
    }
    public int GetReciever() {
        return recieverAccount;
    }
    public double GetAmount() {
        return amount;
    }
    public int GetDOB() {
        return dob;
    }

    public TransactionType GetTransactionType() {
        return transactionType;
    }
    #endregion
}