using System.Collections.Generic;
using UnityEngine;

public class CompanyDB {
    Dictionary<int, Company> companies;
    List<string> names;

    // Singleton
    private static CompanyDB _instance = null;
    public static CompanyDB Instance {
        get {
            if (_instance == null) {
                _instance = new CompanyDB();
            }

            return _instance;
        }
    }

    // Constructor
    CompanyDB() {
        companies = new Dictionary<int, Company>();

        names = new List<string>(Utility.ImportTextFile("/Names/CompanyNames.txt", false, false));
    }

    #region Getters
    public IReadOnlyDictionary<int, Company> GetCompanies() {
        return companies;
    }
    public Company GetCompany(int key) {
        if (companies.ContainsKey(key)) {
            return companies[key];
        }

        MDebug.Warning("CompanyDB", "GetCompany", "Company doesn't exist!", new object[] { key });
        return null;
    }
    #endregion

    // Creates new company
    public int CreateCompany(string name = "") {
        // Id
        int id = Utility.GenerateID();
        while (companies.ContainsKey(id)) {
            id = Utility.GenerateID();
        }

        // Name
        if(name == "") {
            while (name == "") {
                name = GetRandomCompanyName();
                // Search every company, if one is found with same name, set variable to ""
                // if variable is "" then new name is generated otherwise loop breaks and name is decided
                foreach (int key in companies.Keys) {
                    if (companies[key].GetName() == name) {
                        name = "";
                    }
                    break;
                }
                if (name != "") {
                    break;
                }
            }
        }

        // BankAccount
        int bankAccount = Bank.Instance.CreateAccount(id);

        // Logo
        //TODO: decide random logo
        int logo = 0;

        // Create company and return ID
        companies.Add(id, new Company(id, name, bankAccount, logo));
        return id;
    }

    public string GetRandomCompanyName() {
        return names[Random.Range(0, names.Count)];
    }
}

public class Company {
    // General
    int companyId;
    string name;
    int bankAccountId;

    // Ownership
    int parentCompany;
    List<int> sisterCompanies;

    // Employees
    List<int> employees;

    // Logo
    int logo;

    // Constructor
    public Company(int companyId, string name, int bankAccountId, int logo) {
        this.companyId = companyId;
        this.name = name;
        this.bankAccountId = bankAccountId;

        parentCompany = -1;
        sisterCompanies = new List<int>();

        employees = new List<int>();

        this.logo = logo;
    }

    #region Getters
    // General
    public int GetCompanyID() {
        return companyId;
    }
    public string GetName() {
        return name;
    }
    public int GetBankAccountID() {
        return bankAccountId;
    }

    // Ownership
    public int GetParentCompany() {
        return parentCompany;
    }
    public IReadOnlyList<int> GetSisterCompanies() {
        return sisterCompanies;
    }

    // Employees
    public IReadOnlyList<int> GetEmployees() {
        return employees;
    }

    // Logo
    public int GetLogo() {
        return logo;
    }
    #endregion
}
