using UnityEngine;

public class ReferenceDatabase {
    // References to scripts
    public TimeController tc;
    public UI ui;

    // Singleton
    private static ReferenceDatabase _instance = null;
    public static ReferenceDatabase Instance {
        get {
            if (_instance == null) {
                _instance = new ReferenceDatabase();
            }

            return _instance;
        }
    }

    // References scripts at awake
    ReferenceDatabase() {
        tc = GameObject.FindGameObjectWithTag("TimeController").GetComponent<TimeController>();
        ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UI>();

        TestReferences();
    }

    // Tests if all references are found and reports null references
    private void TestReferences() {
        if(tc == null) {
            MDebug.Error("ReferenceDatabase", "TestReferences", "TimeController not found!");
        }
        if (ui == null) {
            MDebug.Error("ReferenceDatabase", "TestReferences", "UI not found!");
        }
    }
}
