using UnityEngine;
using System;

public class TimeController : MonoBehaviour {
    ReferenceDatabase rd;

    public event EventHandler TimeTick;   // Event which other scripts can subscribe to and do their code
    double nextTick = 0;   // At which time will next tick happen
    bool paused = true;   // Is time paused
    TickSpeed tickSpeed = TickSpeed.Normal;   // At what rate does time pass

    // Current date
    int day;
    int week;
    int year;

    private void Start() {
        // Setup date
        day = ConfigTime.StartingDay;
        week = ConfigTime.StartingWeek;
        year = ConfigTime.StartingYear;

        rd = ReferenceDatabase.Instance;
        rd.ui.UpdateUI();
    }

    private void Update() {
        if (!paused) {
            if(Time.time >= nextTick) {
                NextDay();
                TimeTick?.Invoke(this, EventArgs.Empty);
                nextTick = Time.time + TickAdd();               
            }
        }
    }

    #region TimeControl
    // Checks
    public TickSpeed GetSpeed() {
        return tickSpeed;
    }
    public bool IsPaused() {
        return paused;
    }

    // Controls
    public void PauseTime() {
        paused = true;
        rd.ui.UpdateUI();
    }
    public void UnpauseTime() {
        paused = false;
        rd.ui.UpdateUI();
    }
    public void PauseToggle() {
        paused = !paused;
        rd.ui.UpdateUI();
    }
    public void ChangeSpeed(TickSpeed tickSpeed, bool toggle = true, bool unpause = true) {
        //Toggle - should speed be forced or toggle pause if already set at that speed
        //Unpause - should time start ticking if changing to this speed
        if (toggle) {
            if (this.tickSpeed == tickSpeed) {
                PauseToggle();
            }
            else {
                if (unpause) {
                    UnpauseTime();
                }
                this.tickSpeed = tickSpeed;
            }
        }
        else {
            if (unpause) {
                UnpauseTime();
            }
            this.tickSpeed = tickSpeed;
        }

        rd.ui.UpdateUI();
    }
    #endregion

    #region DateControl
    public int GetDOB() {
        return Utility.GetDOB(day, week, year);
    }
    public int GetDay() {
        return day;
    }
    public int GetWeek() {
        return week;
    }
    public int GetYear() {
        return year;
    }

    void NextDay() {
        day++;
        if(day > ConfigTime.WeekDays) {
            day = 1;
            NextWeek();
        }

        rd.ui.UpdateUI();
    }
    void NextWeek() {
        week++;
        if(week > ConfigTime.Weeks) {
            week = 1;
            NextYear();
        }
    }
    void NextYear() {
        year++;
    }
    #endregion

    // Returns speed of tick
    private double TickAdd() {
        return 1 / (double)tickSpeed;
    }
}
