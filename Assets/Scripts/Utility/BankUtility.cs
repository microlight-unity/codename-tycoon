using System;

public class BankUtility {
    // Loan values
    public static double[] CalculateLoan(double amount, int timeYears) {
        // First get rough estimate how much you need to return total and interest
        double totalReturn = LoanTotalReturn(amount, timeYears, ConfigBank.LoanInterest);
        totalReturn = Math.Round(totalReturn * 100) / 100;
        double interest = totalReturn - amount;

        // Then weekly returns
        double weeklyCapitalRate = amount / (timeYears * ConfigTime.Weeks);
        double weeklyInterestRate = interest / (timeYears * ConfigTime.Weeks);

        weeklyCapitalRate = Math.Round(weeklyCapitalRate * 100) / 100;
        weeklyInterestRate = Math.Round(weeklyInterestRate * 100) / 100;

        return new double[] { totalReturn, interest, weeklyCapitalRate, weeklyInterestRate };
    }

    // Formula from wikipedia on loans
    public static double LoanTotalReturn(double loanAmount, int timeYears, double yearlyInterestRate) {
        return loanAmount * ((yearlyInterestRate * Math.Pow((1 + yearlyInterestRate), timeYears)) / (Math.Pow((1 + yearlyInterestRate), timeYears) - 1));
    }
}
