using UnityEngine;

public class UI : MonoBehaviour {
    // Screens
    public UIMain uiMain;
    public UIBank uiBank;

    private void Start() {
        UpdateUI();
    }

    public void UpdateUI() {
        uiMain.UpdateUI();
        uiBank.UpdateUI();
    }
}