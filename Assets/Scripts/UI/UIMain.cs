using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIMain : MonoBehaviour {
    ReferenceDatabase rd;

    [Header("Screen")]
    public GameObject Screen_MainScreen;

    [Header("Buttons")]
    public Button Button_PauseSpeed;
    public Button Button_NormalSpeed;
    public Button Button_FastSpeed;
    public Button Button_ExtraSpeed;

    [Header("Text")]
    public TMP_Text Text_Date;

    private void Awake() {
        rd = ReferenceDatabase.Instance;    
    }

    public void UpdateUI() {
        if (!Screen_MainScreen.activeSelf) {
            return;
        }

        // Pause
        if (rd.tc.IsPaused()) {
            ConfigColors.ButtonRed(Button_PauseSpeed);
        }
        else {
            ConfigColors.ButtonGray(Button_PauseSpeed);
        }

        // Speed
        ConfigColors.ButtonGray(Button_NormalSpeed);
        ConfigColors.ButtonGray(Button_FastSpeed);
        ConfigColors.ButtonGray(Button_ExtraSpeed);

        TickSpeed speed = rd.tc.GetSpeed();
        if (speed == TickSpeed.Normal) {
            ConfigColors.ButtonBlue(Button_NormalSpeed);
        }
        else if (speed == TickSpeed.Fast) {
            ConfigColors.ButtonBlue(Button_FastSpeed);
        }
        else if (speed == TickSpeed.Extra) {
            ConfigColors.ButtonBlue(Button_ExtraSpeed);
        }

        // Date
        Text_Date.text = "Year: " + rd.tc.GetYear() + "; Week: " + rd.tc.GetWeek() + "; Day: " + rd.tc.GetDay();
    }
}
