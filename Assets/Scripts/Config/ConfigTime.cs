public enum TickSpeed {
    // Speed is detrmined by dividing 1 with this number
    Normal = 1,
    Fast = 2,
    Extra = 4
}

public class ConfigTime {
    // Date
    public static int Weeks => 52;
    public static int WeekDays => 5;

    // Starting values
    public static int StartingDay => 1;
    public static int StartingWeek => 1;
    public static int StartingYear => 1970;
}