public class ConfigBank {
    // General
    public static string BankName => "Microlight Bank";   // Bank name

    // Loan
    public static float LoanInterest => 0.03f;   // Yearly interest
    public static float InstantReturnInterest => 0.03f;   // Interest paid on remanaing loan capital for instant loan repayment
    public static int MinLoanTime => 1;   // Minimum time for loan for repayment(in years)
    public static int MaxLoanTime => 15;   // Maximum time for loan repayment (in years)
    public static double MinLoanAmount => 1000;   // Minimum amount that can be loaned
    public static double MaxLoanAmount => 10000000;   // Maximum amount that can be loaned

    // Deficit (when balance is below 0)
    public static float DeficitInterest => 0.06f;   //
}