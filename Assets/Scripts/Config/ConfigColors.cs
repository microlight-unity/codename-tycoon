using UnityEngine;
using UnityEngine.UI;

public class ConfigColors {
    // Colors
    public static Color White => new Color(1f, 1f, 1f);
    public static Color Gray => new Color(0.5f, 0.5f, 0.5f);
    public static Color LightGray => new Color(0.75f, 0.75f, 0.75f);
    public static Color Green => new Color(0f, 0.75f, 0f);
    public static Color DarkGreen => new Color(0f, 0.5f, 0f);
    public static Color LightGreen => new Color(0f, 1f, 0f);

    #region Buttons
    // Specific button colors
    public static void ButtonRed(Button button) {
        ColorBlock cb = button.colors;

        cb.normalColor = new Color(1f, 0.3f, 0.3f, 1f);
        cb.highlightedColor = new Color(0.9f, 0.2f, 0.2f, 1f);
        cb.pressedColor = new Color(0.7f, 0.08f, 0.08f, 1f);
        cb.selectedColor = new Color(1f, 0.3f, 0.3f, 1f);
        cb.disabledColor = new Color(0.7f, 0.08f, 0.08f, 0.5f);

        button.colors = cb;
    }
    public static void ButtonGreen(Button button) {
        ColorBlock cb = button.colors;

        cb.normalColor = new Color(0.3f, 0.85f, 0.3f, 1f);
        cb.highlightedColor = new Color(0.25f, 0.75f, 0.25f, 1f);
        cb.pressedColor = new Color(0.2f, 0.65f, 0.2f, 1f);
        cb.selectedColor = new Color(0.3f, 0.85f, 0.3f, 1f);
        cb.disabledColor = new Color(0.2f, 0.65f, 0.2f, 0.5f);

        button.colors = cb;
    }
    public static void ButtonBlue(Button button) {
        ColorBlock cb = button.colors;

        cb.normalColor = new Color(0.3f, 0.85f, 0.85f, 1f);
        cb.highlightedColor = new Color(0.25f, 0.75f, 0.75f, 1f);
        cb.pressedColor = new Color(0.2f, 0.65f, 0.65f, 1f);
        cb.selectedColor = new Color(0.3f, 0.85f, 0.85f, 1f);
        cb.disabledColor = new Color(0.2f, 0.65f, 0.65f, 0.5f);

        button.colors = cb;
    }
    public static void ButtonGray(Button button) {
        ColorBlock cb = button.colors;

        cb.normalColor = new Color(1f, 1f, 1f, 1f);
        cb.highlightedColor = new Color(0.95f, 0.95f, 0.95f, 1f);
        cb.pressedColor = new Color(0.75f, 0.75f, 0.75f, 1f);
        cb.selectedColor = new Color(1f, 1f, 1f, 1f);
        cb.disabledColor = new Color(0.75f, 0.75f, 0.75f, 0.5f);

        button.colors = cb;
    }
    #endregion
}
